# Test Server.
from os import environ

from arrow import now
from flask import Response, jsonify, make_response, request

# from api import create_api, db
from api import create_api
# from api.manager import AuthManager, BuyOrderManager
from api.manager import AuthManager
from logger import logger

if environ.get('FLASK_ENV') is None:
	print('FLASK_ENV not set')
mode = environ.get('FLASK_ENV') or 'Staging'
app = create_api(mode)


@app.after_request
def log_info(response):
	try:
		log_data = {
			'connection': request.headers.get('Connection'),
			'ip_address': request.remote_addr if hasattr(response, 'remote_addr') else None,
			'browser_name': request.user_agent.browser if hasattr(response, 'user_agent') else None,
			'user_device': request.user_agent.platform if hasattr(response, 'user_agent') else None,
			'referrer': request.referrer if hasattr(response, 'referrer') else None,
			'request_url': request.url if hasattr(response, 'url') else None,
			'host_url': request.host_url if hasattr(response, 'host_url') else None,
			'status_code': response.status_code if hasattr(response, 'status_code') else None,
			'date': str(now('Africa/Lagos')),
			'location': response.location if hasattr(response, 'location') else None,
		}
		logger.info('auth_api_logs : {}'.format(log_data))
	except Exception as e:
		logger.exception("auth_api_logs: {}".format(e))
	return response


# with app.app_context():
# 	# db.reflect()
# 	# db.drop_all()
# 	db.create_all()


@app.after_request
def verify(response):
	if request.method == 'OPTIONS':
		# Hack to prevent CORS errors
		# Please look into this as the original authors aren't too sure what is going on here
		resp = {}
		resp['success'] = True
		resp['message'] = 'DON\'T use OPTIONS in any of the app routes'
		response = Response(resp, 200, {})
		return response

	exempted = {
		'/doc/',
		'/auth/login/',
		'/auth/signup/',
		'/auth/token-check/',
		'/auth/whoami/',
		'/authy/verify/',
		'/configuration/demo/',
		'/cmdctrl',
		'/account/credit/'
		# '/buy/self-new-order/',
		# '/buy/admin-new-order/',
		# '/buy/edit-order/',
	}
	if request.path.startswith('/invite/accept/'):
		manager = AuthManager()
		resp = manager.invite_accept()
		return resp

	if request.path.startswith('/invite/resend/'):
		manager = AuthManager()
		resp = manager.invite_resend()
		logger.info("\n\n\n\tAFTER RESP = {}".format(resp))
		return resp

	if request.path.startswith('/configuration/demo/'):
		manager = AuthManager()
		resp = manager.reroute(use_claims=False)
		logger.info("\n\n\n\tAFTER RESP = {}".format(resp))
		return resp

	if request.path.startswith('/account/credit/'):
			manager = AuthManager()
			resp = manager.reroute(use_claims=False)
			logger.info("\n\n\n\tAFTER RESP = {}".format(resp))
			return resp


	# if request.path.startswith('/buy/'):
	# 	if request.path in exempted:
	# 		manager = BuyOrderManager()
	# 		resp = manager.run()
	# 		logger.info("\n\n\n\tAFTER RESP = {}".format(resp))
	# 		return resp

	if request.path not in exempted:
		manager = AuthManager()
		path = manager.get_path()
		login_path = '/auth/login/'
		claims, status_code = manager.get_claims()

		if status_code == 201:
			if path == login_path:
				resp = {}
				resp['success'] = False
				resp['message'] = 'Already Logged In.'
				status = 409
				return resp, status
			resp = manager.reroute(claims=claims)
			return resp
		if status_code == 401:
			# User not logged in
			if response.status_code == 404:
				# If authentication failed, the requested url is viewed as invalid, hence the not found error.
				claims = jsonify(claims)
				response = make_response(claims, status_code)
				return response

			return response
	return response
