import os


class Config(object):
	DEBUG = False
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	SECRET_KEY = os.environ['SECRET_KEY']
	JWT_KEY = os.environ['JWT_KEY']
	LOGIN_URL = os.environ['LOGIN_URL']
	SIGNUP_URL = os.environ['SIGNUP_URL']
	PROXY_URL = os.environ['PROXY_URL']
	SIGNUP_PAGE = os.environ['SIGNUP_PAGE']
	HOME_PAGE = os.environ['HOME_PAGE']
	INVITE_ACCEPT = os.environ['INVITE_ACCEPT']

	ACCOUNT_BALANCE_URL = os.environ['ACCOUNT_BALANCE_URL']
	BUYORDER_SERVICE = os.environ['BUYORDER_SERVICE']
	BILLING_UNIT_URL = os.environ['BILLING_UNIT_URL']

	MY_CONFIGURATION_IDS_URL = os.environ['MY_CONFIGURATION_IDS_URL']

	RESEND_INVITE_PAGE = os.environ['RESEND_INVITE_PAGE']
	# SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']

	@staticmethod
	def init_app(app):
		pass


class Development(Config):
	DEBUG = True
	JWT_SECRET_KEY = os.urandom(3)

	def __repr__(self):
		return '{}'.format(self.__class__.__name__)

	@staticmethod
	def init_app(app):
		pass


class Testing(Config):
	"""TODO"""
	pass


class Staging(Config):
	"""TODO"""

	def __repr__(self):
		return '{}'.format(self.__class__.__name__)


class Production(Config):
	def __repr__(self):
		return '{}'.format(self.__class__.__name__)


config = {
	'Development': Development,
	'Testing': Testing,
	'Production': Production,
	'Staging': Staging,

	'default': Development,


}
