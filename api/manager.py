import json
from time import time

import jwt
import requests
from flask import current_app, redirect, request

from api.tokens import Token
from logger import logger


class BuyOrderManager:

	def get_account_balance(self, payload):
		group_id = payload['user_group_id']
		account_api = current_app.config.get('ACCOUNT_BALANCE_URL')
		resp = requests.get(account_api.format(group_id=group_id), cookies={'role_id': '6'})
		resp = resp.json()
		account_balance = resp['account_balance']
		return account_balance

	def perform_buyorder(self, payload):
		buyorder_service = current_app.config.get('BUYORDER_SERVICE')
		full_url = buyorder_service + request.path
		method = request.method
		response = requests.request(method, full_url, data=payload, cookies=request.cookies)
		return response

	def get_billing_unit(self, billing_unit_id):
		billing_unit_url = current_app.config.get('BILLING_UNIT_URL')
		url = billing_unit_url.format(billing_unit_id=billing_unit_id)
		resp = requests.get(url, cookies={'role_id': '6'})
		name = resp.json()['billing_unit_name']
		return name.title()

	def get_order_amount(self, payload):
		order_amount = 0
		# consumption_forecast = 500.0 # in Watt-hours
		# billing_units = ['Energy', 'Time']
		logger.info(payload)
		try:
			billing_unit_id = payload['billing_unit_id']
		except KeyError as e:
			logger.exception(e)
			print(payload)
			logger.info(payload)
			raise
		billing_unit = self.get_billing_unit(billing_unit_id)
		if billing_unit == 'Time':
			order_amount = payload['rate']
		# elif billing_unit == 'Energy':
		# 	rate = payload['rate']
		# 	order_amount = rate * consumption_forecast
		return order_amount

	def run(self):
		payload = request.values.to_dict() or request.json
		order_amount = self.get_order_amount(payload)
		account_balance = self.get_account_balance(payload)
		if account_balance >= order_amount:
			resp = self.perform_buyorder(payload)
			return resp
		account_response = {
			'message': 'Insufficient funds.',
			'pending_amount': order_amount - account_balance,
			'buy_order_data': payload
		}
		response = current_app.response_class(
			response=json.dumps(account_response),
			status=409,  # see https://softwareengineering.stackexchange.com/a/341824
			mimetype='application/json'
		)
		return response


class AuthManager(object):
	"""docstring for AuthManager"""

	def get_path(self):
		return request.path

	def get_claims(self, token_check=False):
		token = None
		payload = {}
		if request.path.startswith(current_app.config['INVITE_ACCEPT']):
			payload['success'] = True
			payload['message'] = 'Invite Accepted...'
			return payload, 201

		if 'X-API-KEY' in request.headers:
			token = request.headers['X-API-KEY']

		if token is None:
			payload['success'] = False
			payload['message'] = 'Token is missing'
			return payload, 401

		claims, is_valid = Token.decode_auth_token(token)
		token_check = False
		is_valid = True
		if token_check:
			if is_valid:
				payload['success'] = True
				payload['message'] = 'Token Valid'
				return payload, 201
			payload['success'] = False
			payload['message'] = claims
			return payload, 401

		if is_valid:
			return claims, 201

		payload['success'] = False
		payload['message'] = claims
		return payload, 401

	def reroute(self, claims=None, use_claims=True):
		t0 = time()
		proxy_url = current_app.config['PROXY_URL']
		path = request.path

		url = proxy_url + path
		print("\n\n URL = ", url)

		cookies = {str(k): str(v) for (k, v) in request.cookies.items()}
		if use_claims:
			if not isinstance(claims, dict):
				bad_request = {}
				bad_request['success'] = False
				bad_request['message'] = claims
				response = current_app.response_class(
					response=json.dumps(bad_request),
					status=401,
					mimetype='application/json'
				)
				return response
			claims = {str(k): str(v) for (k, v) in claims.items()}
			cookies.update(claims)
			# print("\n\n\nCOOKIES = ", cookies)
			# print("\n\n\nCLAIMS = ", claims)
			verified_claims = ['user_id', 'group_id', 'role_id', 'email', 'facilities']
			invalid_claims = {}
			invalid_claims['success'] = True
			invalid_claims['message'] = 'Unable to verifiy your identity. Please login again.'
			try:
				if not any(claims[key] for key in verified_claims):
					response = current_app.response_class(
						response=json.dumps(invalid_claims),
						status=401,
						mimetype='application/json'
					)
					return response
			except Exception:
				print('\n\n\n claims ', claims)
		t1 = time()
		resp = requests.request(
			method=request.method,
			url=url,
			headers={key: value for (key, value) in request.headers if key != 'Host'},
			data=request.get_data(),
			cookies=cookies,
			params={str(k): str(v) for (k, v) in request.args.items()},
			allow_redirects=False
		)
		t2 = time()

		excluded_headers = [
			'content-encoding',
			'content-length',
			'transfer-encoding',
			'connection'
		]
		headers = [
			(name, value)
			for (name, value)
			in resp.raw.headers.items()
			if name.lower() not in excluded_headers
		]

		response = current_app.response_class(
						response=resp.content,
						status=resp.status_code,
						mimetype='application/json',
						headers=headers
					)
		t3 = time()
		request_time = t2 - t1
		proxy_time = t3 - t0
		proxy_overhead = proxy_time - request_time
		print("\n {} request to {} took {}s\t\tProxy Overhead = {}s".format(
			request.method,
			url,
			request_time,
			proxy_overhead,
			)
		)
		return response

	def login(self, payload):
		response = {}
		proxy_url = current_app.config['PROXY_URL']
		login_url = proxy_url + request.path
		print('\n\n\n\n\n')
		print(login_url)
		resp = requests.post(login_url, data=payload)
		code = resp.status_code
		try:
			resp = resp.json()
			if code == 200:
				# """MFA REQUIRED"""
				token_params = AuthManager.get_token_params(resp, mfa=True)
				token, success = Token.encode_auth_token(token_params)
				if success:
					response['success'] = success
					response['token'] = token
					response['mfa_user'] = resp['data'].get('mfa_user')
					response['authy_id'] = resp['data'].get('authy_id')
					# Explicitly require MFA as a 200 response may be used for other authentication requests.
					response['require_mfa'] = resp['data'].get('mfa_user')
					return response, 200
				response['success'] = success
				response['message'] = 'Token Generation failed. Try Again.'
				return response, 401

			elif code == 201:
				# """MFA SUCCESS or MFA NOT REQUIRED"""
				token_params = AuthManager.get_token_params(resp)
				token, success = Token.encode_auth_token(token_params)
				if success:
					response['success'] = success
					response['token'] = token
					response['data'] = resp['data']
					return response, code

				response['success'] = success
				response['message'] = 'Token Generation failed. Try Again.'
				return response, 401
			else:
				# """Unsuccessful login"""
				response = resp
				return response, 401
		except Exception as e:
			logger.exception(e)
			message = 'Authentication Error'
			response['success'] = False
			response['message'] = message
			return response, 500

	def signup(self, payload):
		response = {}
		proxy_url = current_app.config['PROXY_URL']
		token = payload.get('token')
		del payload['token']
		claims, is_valid = Token.decode_auth_token(token)
		if is_valid:
			signup_url = proxy_url + request.path
			payload['email'] = claims['email']
			resp = requests.post(signup_url, data=payload)
			status_code = resp.status_code
			if status_code == 200:
				# """MFA REQUIRED"""
				resp = resp.json()
				token_params = AuthManager.get_token_params(resp, mfa=True)
				token, success = Token.encode_auth_token(token_params)
				if success:
					response['success'] = success
					response['token'] = token
					response['mfa_user'] = resp['data'].get('mfa_user')
					response['authy_id'] = resp['data'].get('authy_id')
					response['require_mfa'] = resp['data'].get('mfa_user')
					return response, 200
				response['success'] = success
				response['message'] = 'Token Generation failed. Try Again.'
				return response, 401

			if status_code == 201:
				# """MFA SUCCESS or MFA NOT REQUIRED"""
				resp = resp.json()
				token_params = AuthManager.get_token_params(resp)
				token, success = Token.encode_auth_token(token_params)
				if success:
					response['success'] = success
					response['token'] = token
					response['data'] = resp['data']
					return response, status_code
				response['success'] = success
				response['message'] = 'Token Generation failed. Try Again.'
				return response, 401
			else:
				# """Unsuccessful signup"""
				response = resp.json()
				return response, resp.status_code
		logger.info('Invalid tokens {}'.format(claims))
		response['success'] = False
		response['message'] = 'Invalid Token'
		return response, 400

	def get_sudo(self):
		response = {}
		proxy_url = current_app.config['PROXY_URL']
		sudo_url = proxy_url + request.path
		print('\n\n\n')
		logger.info(sudo_url)
		resp = requests.get(sudo_url)
		status_code = resp.status_code
		if status_code == 201:
			# """MFA SUCCESS or MFA NOT REQUIRED"""
			resp = resp.json()
			token_params = AuthManager.get_token_params(resp)
			token, success = Token.encode_auth_token(token_params)
			if success:
				response['success'] = success
				response['token'] = token
				return response, status_code
			response['success'] = success
			response['message'] = 'Token Generation failed. Try Again.'
			return response, 401
		response = resp.json()
		return response, resp.status_code

	def invite_accept(self):
		response = {}
		proxy_url = current_app.config['PROXY_URL']
		path_list = request.path.split("/")
		token = path_list[-2]
		logger.info("Obtaining user email")
		logger.info("PATH LIST = {}".format(path_list))
		logger.info("REQUEST PATH = {}".format(request.path))
		logger.info("TOKEN = {}".format(token))
		try:
			claims, is_valid = Token.decode_auth_token(token, propagate=True)
		except jwt.ExpiredSignatureError:
			claims, _ = Token.decode_without_verification(token)
			email = claims['email']
			token_params = {'email': email}
			token, _ = Token.encode_auth_token(token_params, expire=False)
			response = redirect(current_app.config['RESEND_INVITE_PAGE'] + "?key={}".format(token))
			return response

		logger.info("Got Claims {}".format(claims))
		if is_valid:
			email = claims['email']
			logger.info("User email = {}".format(email))
			path_list[-2] = email
			new_path = "/".join(path_list)
			invite_accept_url = proxy_url + new_path
			resp = requests.get(invite_accept_url)
			logger.info("\n\n\tResponse = {}".format(resp))
			if resp.status_code == 200:
				body = resp.json()
				email = body.get('email')
				token_params = {'email': email}
				token, _ = Token.encode_auth_token(token_params, expire=False)
				response = redirect(current_app.config['SIGNUP_PAGE'] + "?key={}".format(token))
				return response

		response = redirect(current_app.config['HOME_PAGE'])
		return response

	def invite_resend(self):
		response = {}
		proxy_url = current_app.config['PROXY_URL']
		token = request.args.get("key")
		try:
			claims, is_valid = Token.decode_auth_token(token, propagate=True)
		except jwt.ExpiredSignatureError as e:
			logger.exception(e)
			logger.info("\n\n\n\n")
			logger.info("Uncovered exception")
			logger.info("\n\n\n\n")

		logger.info("Got Claims {}".format(claims))
		if is_valid:
			email = claims['email']
			logger.info("User email = {}".format(email))
			invite_resend_url = proxy_url + request.path + "?email={}".format(email)
			resp = requests.get(invite_resend_url)

			excluded_headers = [
				'content-encoding',
				'content-length',
				'transfer-encoding',
				'connection'
			]
			headers = [
				(name, value)
				for (name, value)
				in resp.raw.headers.items()
				if name.lower() not in excluded_headers
			]

			response = current_app.response_class(
							response=resp.content,
							status=resp.status_code,
							mimetype='application/json',
							headers=headers
						)
			logger.info("\n\n\tResponse = {}".format(response))
			return response

		response = redirect(current_app.config['HOME_PAGE'])
		return response

	def authy_verify(self, payload):
		response = {}
		cookies = {str(k): str(v) for (k, v) in request.cookies.items()}
		claims = {str(k): str(v) for (k, v) in AuthManager().get_claims()[0].items()}
		cookies.update(claims)
		logger.info("\n\n\n\t\t AUTHY CLAIMS = {}".format(cookies))
		proxy_url = current_app.config['PROXY_URL']
		authy_url = proxy_url + request.path
		resp = requests.post(authy_url, data=payload, cookies=cookies)
		if resp.status_code == 201:
			resp = resp.json()
			token_params = AuthManager.get_token_params(resp)
			token, success = Token.encode_auth_token(token_params)
			if success:
				response['success'] = success
				response['token'] = token
				response['data'] = resp['data']
				return response, 201
			response['success'] = False
			response['message'] = 'Token generation failed. Please Login.'
			return response, 401
		try:
			response = resp.json()
		except Exception as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'MFA Authentication Error'
		return response, 401

	@staticmethod
	def get_token_params(resp, mfa=False):
		configuration_ids = []
		config_user = resp['data']['configuration_user']
		if config_user:
			my_config_id_url = current_app.config['MY_CONFIGURATION_IDS_URL']
			user_id = resp['data']['user_id']
			config_resp = requests.get(my_config_id_url, cookies={'user_id': user_id})
			if config_resp.ok:
				configuration_ids = config_resp.json()['data']

		if mfa:
			token_params = {
				"user_id": resp['data']['user_id'],
				"email": str(resp['data']['email']),
			}
			return token_params

		my_facilities = [facility['facility_id'] for facility in resp['data']['facilities']]
		# facilities = ' '.join(str(facility) for facility in my_facilities)
		token_params = {
			"user_id": resp['data']['user_id'],
			"group_id": resp['data']['group_id'],
			"role_id": str(resp['data']['role_id']),
			"email": str(resp['data']['email']),
			"facilities": my_facilities,
			"configuration_ids": configuration_ids,
		}
		return token_params
