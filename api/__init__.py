from flask import Flask
# from flask_sqlalchemy import SQLAlchemy
from flask_restplus import Api
from flask_cors import CORS

from config import config

# db = SQLAlchemy()
api = Api(doc='/doc/')
cors = CORS()

def create_api(config_name):
	app = Flask(__name__)

	try:
		init_config = config[config_name]()
	except KeyError:
		raise
	except Exception:
		# For unforseen exceptions
		raise

	print('Running in {} Mode'.format(init_config))
	config_object = config.get(config_name)

	app.config.from_object(config_object)

	# db.init_app(app)
	from api.controllers import auth_api as ns1
	from api.controllers import authy_api as ns2
	api.add_namespace(ns1, path='/auth')
	api.add_namespace(ns2, path='/authy')

	api.init_app(app)
	cors.init_app(app)


	return app
