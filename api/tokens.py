import jwt
from flask import current_app
# from api.models import RevokedToken
from datetime import datetime, timedelta


class Token(object):

	@staticmethod
	def encode_auth_token(user_params, expire=True):
		"""
		Generates the Auth Token
		:return: string
		"""
		success = False
		secret_key = current_app.config.get('JWT_KEY')
		try:
			payload = {
				'iat': datetime.utcnow(),
				'sub': user_params
			}
			if expire:
				payload['exp'] = datetime.utcnow() + timedelta(days=1, seconds=5)

			token = jwt.encode(
				payload,
				secret_key,
				algorithm='HS256'
			).decode('utf-8')
			success = True
			return token, success
		except Exception as e:
			return e, success

	@staticmethod
	def marshal_payload(payload):
		return payload['sub']

	@staticmethod
	def decode_auth_token(auth_token, propagate=False):
		"""
		Validates the auth token
		:param auth_token:
		:return: dict
		"""
		secret_key = current_app.config.get('JWT_KEY')
		try:
			payload = jwt.decode(auth_token, secret_key)
			# is_revoked = RevokedToken.check_revoked(auth_token)
			# if is_revoked:
			# 	return 'Token revoked. Please log in.', False
			return Token.marshal_payload(payload), True
		except jwt.ExpiredSignatureError:
			if propagate:
				raise
			return 'Signature expired. Please log in.', False
		except jwt.InvalidTokenError:
			return 'Invalid token. Please log in.', False

	@staticmethod
	def decode_without_verification(auth_token):
		"""
		Obtains the claims from a token without verifying the token.
		Be careful with this usage.
		Current usage is in obtaining a user's email when an invite is expired.
		:param auth_token:
		:return: dict
		"""
		try:
			payload = jwt.decode(auth_token, verify=False)
			return Token.marshal_payload(payload), True
		except jwt.InvalidTokenError:
			return 'Invalid token. Please log in.', False
