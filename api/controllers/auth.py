from api import api
from api.manager import AuthManager
from api.schema import LoginSchema
from flask import request
from flask_restplus import Namespace, Resource, fields
from logger import logger
from marshmallow import ValidationError

auth_api = Namespace('auth', description='Api for authenticating and validating users')

login = auth_api.model('Login', {
	'user_id': fields.String(description='The user\'s email address or phone number'),
	'password': fields.String(required=True, description='The user\'s password'),
})

new_user = auth_api.model('NewUser', {
	'first_name': fields.String(required=True, description='The user\'s first name'),
	'last_name': fields.String(required=True, description='The user\'s last name'),
	'email': fields.String(description='(Optional) The user\'s email address'),
	'password': fields.String(required=True, description='User\'s password'),
	'confirm': fields.String(required=True, description='User\'s password'),
	'phone_number': fields.String(required=True, description='The user\'s phone number'),
})


@auth_api.route('/login/')
class Login(Resource):
	"""
		Api for user login
	"""

	@auth_api.expect(login)
	def post(self):
		"""
			HTTP method for user login
			@param: payload: data for the update
			@returns: response and status code
		"""
		schema = LoginSchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = AuthManager()
		resp, code = manager.login(payload)
		return resp, code


@auth_api.route('/signup/')
class SignUp(Resource):
	"""
		Api for user login
	"""

	@auth_api.expect(new_user)
	def post(self):
		"""
			HTTP method for user signup
			@param: payload: data for the update
			@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data

		manager = AuthManager()
		resp, code = manager.signup(payload)
		return resp, code


@auth_api.route('/sudo/')
class Sudo(Resource):
	"""
		Api for user login
	"""

	@auth_api.expect(new_user)
	def get(self):
		"""
			HTTP method to get a sudo user. Delete in production. Only used by cmdctrl
			@param: payload: data for the update
			@returns: response and status code
		"""
		manager = AuthManager()
		resp, code = manager.get_sudo()
		return resp, code


@auth_api.route('/whoami/')
class WhoAmI(Resource):
	"""
		Api for user login
	"""

	def get(self):
		"""
			HTTP method to get user claims
			@param: payload: data for the update
			@returns: response and status code
		"""
		manager = AuthManager()
		resp, code = manager.get_claims()
		return resp, code


@auth_api.route('/token-check/')
class TokenCheck(Resource):
	"""
		Api for checking user tokens
	"""

	def get(self):
		"""
			Checks if the token in the header is valid.
			@returns: response and status code
		"""
		manager = AuthManager()
		resp, code = manager.get_claims(token_check=True)
		return resp, code
