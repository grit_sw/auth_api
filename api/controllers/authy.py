from api import api
from flask import request
from flask_restplus import Namespace, Resource, fields
from api.schema import AuthySchema
from marshmallow import ValidationError
from logger import logger
from api.manager import AuthManager

authy_api = Namespace('authy', description='Api for validating authy MFA code')

verify = authy_api.model('Verify', {
	'authy_code': fields.String(description='Authy MFA code'),
})


@authy_api.route('/verify/')
class Verify(Resource):
	"""
		Api for user login
	"""

	@authy_api.expect(verify)
	def post(self):
		"""
			HTTP method for mfa validation
			@param: payload: data for the update
			@returns: response and status code
		"""
		schema = AuthySchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = AuthManager()
		resp, code = manager.authy_verify(new_payload)
		return resp, code
