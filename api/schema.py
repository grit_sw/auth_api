from marshmallow import Schema, fields, post_load, validates, ValidationError
from collections import namedtuple
import re

UserLogin = namedtuple('UserLogin', ['user_id', 'password'])
AuthyData = namedtuple('AuthyData', ['authy_code'])


def is_email(email):
	if re.match(r"[^@]+@[^@]+\.[^@]+", email):
		return True
	return False


def is_mobile(phone_number):
	reg_string = r"^(?=(?:.{11}|.{14})$)[+]?[0-9]*$"
	if re.match(reg_string, phone_number):
		return True
	return False


class AuthySchema(Schema):
	authy_code = fields.String(required=True)

	@post_load
	def validate_authy(self, data):
		return AuthyData(**data)

	@validates('authy_code')
	def validate_user_id(self, value):
		if len(value) <= 0:
			raise ValidationError('MFA code required.')


class LoginSchema(Schema):
	user_id = fields.String(required=True)
	password = fields.String(required=True)

	@post_load
	def login_user(self, data):
		return UserLogin(**data)

	@validates('user_id')
	def validate_user_id(self, value):
		if not (is_email(value) or is_mobile(value)):
			raise ValidationError('Email or Phone Number Required')

	@validates('password')
	def validate_password(self, value):
		if len(value) <= 0:
			raise ValidationError('Required')

	def __repr__(self):
		return "User <{}>".format(self.user_id)
